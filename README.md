
# Matemáticas
## Conjuntos, Aplicaciones y funciones (2002)

Video de referencia

[Temas 6 y 7: Conjuntos, Aplicaciones y funciones](https://canal.uned.es/video/5a6f1b77b1111f15098b4609)

```plantuml
@startmindmap
*[#48005E] <font color=#FFFFFF>Conjuntos, Aplicaciones \n<font color=#FFFFFF>y funciones
	
	*_ su 
		*[#732487] <font color=#FFFFFF>Concepto
			*_ no 
				*[#C59FCB] Se define.
			*_ se 
				*[#C59FCB] Considera una idea intuitiva.

	*_ tiene una 
		*[#732487] <font color=#FFFFFF>Teoría
			*_ de 
				*[#C59FCB] Inclusión de conjunto
					*_ se basa 
						*[#FFFFFF] En la relación de orden que hay entre números.
					*_ es 
						*[#FFFFFF] La relación de un subconjunto en un conjunto.

	*_ sus 
		*[#732487] <font color=#FFFFFF>Operaciones de conjuntos
			*_ son
				*[#C59FCB] Intersección
					*_ son
						*[#FFFFFF] Elementos que pertenecen simultáneamente.
				*[#C59FCB] Unión
					*_ son
						*[#FFFFFF] Elementos que pertenecen al menos uno de ellos.
				*[#C59FCB] Complementación
					*_ son
						*[#FFFFFF] Elementos que no pertenecen a un conjunto.
			*_ es
				*[#C59FCB] Aplicada
					*_ en
						*[#FFFFFF] Lógica
						*[#FFFFFF] Probabilidades

	*_ existen
		*[#732487] <font color=#FFFFFF>Tipos
			*_ como
				*[#C59FCB] Universal
					*_ es
						*[#FFFFFF] Un conjunto de referencia.
					*_ en el
						*[#FFFFFF] Ocurre toda cosa de la teoría.	
				*[#C59FCB] Vacío
					*_ es una
						*[#FFFFFF] Necesidad lógica
							*_ de 
								*[#FFFFFF] Conjuntos que no tienen elementos.
			*_ se
				*[#C59FCB] Representan
					*_ por
						*[#FFFFFF] Dibujos
						*[#FFFFFF] Sistemas
							*_ como
								*[#FFFFFF] Círculos.
								*[#FFFFFF] óvalos.
						*[#FFFFFF] Diagramas de Venn

	*_ las
		*[#732487] <font color=#FFFFFF>Propiedades
			*_ son
				*[#C59FCB] Cardinalidad
					*_ es el
						*[#FFFFFF] Número de elementos
							*_ que
								*[#FFFFFF] Forman el conjunto
							*_ se indica con
								*[#FFFFFF] Números naturales
				*[#C59FCB] Características
					*_ la
						*[#FFFFFF] Cardinal de una unión
							*_ de
								*[#FFFFFF] A+B-(intersección)
						*[#FFFFFF] Acotación de cardinales.
							*_ ejemplo
								*[#FFFFFF] mayor, menor, igual entre \ncardinales de conjuntos.

	*_ sus
		*[#732487] <font color=#FFFFFF> Aplicaciones matemáticas
			*_ son
				*[#C59FCB] Transformación
					*_ es la
						*[#FFFFFF] Transformación o regla que convierte \na cada uno de los elementos de un \ndeterminado conjunto.
							*_ llamado
								*[#FFFFFF] Conjunto A.
								*[#FFFFFF] Conjunto inicial.
							*_ en 
								*[#FFFFFF] Un único elemento, llamado conjunto único.
						*[#FFFFFF] Deja de ser aplicación matemática
							*_ si
								*[#FFFFFF] Deja de lado a uno de los elementos.
				*[#C59FCB] Transformación de \nla transformación
					*_ es la
						*[#FFFFFF] Composición de aplicaciones

	*_ los
		*[#732487] <font color=#FFFFFF> Tipos de aplicaciones
			*_ son
				*[#C59FCB] Central o definido
					*_ se
						*[#FFFFFF] Desarrollan
							*[#FFFFFF] Nociones técnicas.
							*[#FFFFFF] Razonamiento abstracto.
				*[#C59FCB] Aplicación inyectiva
				*[#C59FCB] Aplicación subjetiva
				*[#C59FCB] Aplicación biyectiva
				*[#C59FCB] Aplicaciones particulares
					*_ los
						*[#FFFFFF] Conjuntos que se transforman 
							*_ son 
								*[#FFFFFF] Conjuntos de números
									*[#FFFFFF] Naturales.
									*[#FFFFFF] Enteros.
									*[#FFFFFF] Reales.
					*_ ejemplo
						*[#FFFFFF] Cuando se tiene una transformación en un conjunto de números.

	*_ su
		*[#732487] <font color=#FFFFFF> Función
			*_ solo
				*[#C59FCB] Es una terminología.
			*_ es una
				*[#C59FCB] Representación gráfica
					*_ del
						*[#C59FCB] Conjunto de los puntos.
@endmindmap
```

## Funciones (2010)

Video de referencia

[Funciones](https://canal.uned.es/video/5a6f2d09b1111f21778b457f)

```plantuml
@startmindmap
*[#48005E] <font color=#FFFFFF>Funciones
	
	*_ se
		*[#732487] <font color=#FFFFFF>Consideran
			*_ el
				*[#C59FCB] Corazón de las mátematicas.

	*_ reflejan que
		*[#732487] <font color=#FFFFFF>El hombre
			*_ busca
				*[#C59FCB] Herramientas matemáticas.
			*_ puede
				*[#C59FCB] Comprender su entorno.
				*[#C59FCB] Resolver problemas cotidianos.
			*_ enfrenta
				*[#C59FCB] Situación de cambio 
					*_ en el
						*[#FFFFFF] Conocimiento científico.

	*_ tienen un
		*[#732487] <font color=#FFFFFF> concepto
			*_ es
				*[#C59FCB] los conjuntos que se relacionan entre sí, son conjuntos de números
					*_ ejemplo
						*[#FFFFFF] Se toma un número y se eleva al cuadrado
							*_ genera
								*[#FFFFFF] su imagen (resultado).

	*_ su
		*[#732487] <font color=#FFFFFF> Representación
			*_ puede ser
				*[#C59FCB] Gráfica
					*_ como
						*[#FFFFFF] El plano cartesiano
							*_ compuesto por
								*[#FFFFFF] Ejes x, y.
									*_ el
										*[#FFFFFF] Punto x representa la imagen.
								*[#FFFFFF] Números reales.

	*_ tiene
		*[#732487] <font color=#FFFFFF> Características
			*_ son
				*[#C59FCB] Creciente
					*_ es
						*[#FFFFFF] El Intervalo máximo.
							*_ La
								*[#FFFFFF] La variable del primer conjunto aumenta.
				*[#C59FCB] Decreciente
					*_ es
						*[#FFFFFF] El intervalo mínimo
							*_ La
								*[#FFFFFF] Variable disminuye.

	*_ tiene
		*[#732487] <font color=#FFFFFF> Un comportamiento
			*_ en un
				*[#C59FCB] Intervalo
					*_ es
						*[#FFFFFF] Trozo del rango de valores.
		*[#732487] <font color=#FFFFFF> Un límite
			*_ se da cuando
				*[#C59FCB] El valor de X se acerca
					*_ a
						*[#FFFFFF] 0 
						*[#FFFFFF] Otro determinado valor.
				*[#C59FCB] El valor se acerca al cero hay una descontinuidad, un salto.
					*_ es decir
						*[#FFFFFF] La función carece de límite.
		*[#732487] <font color=#FFFFFF> Una continuidad
			*_ sus
				*[#C59FCB] Características
					*_ son 
						*[#FFFFFF] Funciones manejables.
						*[#FFFFFF] No producen saltos.
						*[#FFFFFF] Valores cerca de los valores antiguos.

	*_ el
		*[#732487] <font color=#FFFFFF> Cálculo diferencial
			*_ la
				*[#C59FCB] Derivada 
					*_ es
						*[#FFFFFF] Una función simple (línea recta)
							*_ con
								*[#FFFFFF] Una función complicada (curvas, picos, etc).
					*_ su objetivo
						*[#FFFFFF] Resolver el problema de aproximación de una función compleja.
							*_ mediante 
								*[#FFFFFF] Una función simple (pendiente de una tangente).
					*_ tiene
						*[#FFFFFF] Procesos
							*_ como
								*[#FFFFFF] Velocidad 
							*_ utilizada
								*[#FFFFFF] En tiempos.
					*_ utiliza
						*[#FFFFFF] Cálculos matemáticos
							*_ siguiendo
								*[#FFFFFF] Las reglas básicas.
@endmindmap
```

## La matemática del computador (2002)

Videos de referencia

[La matemática del computador. El examen final](https://canal.uned.es/video/5a6f1b76b1111f15098b45fa)

[Tema 12: Las matemáticas de los computadores. El examen final](https://canal.uned.es/video/5a6f1b81b1111f15098b4640)

```plantuml
@startmindmap
*[#48005E] <font color=#FFFFFF>  La matemática \n<font color=#FFFFFF>del computador
	
	*_ el
		*[#732487] <font color=#FFFFFF>Ordenador
			*_ tiene
				*[#C59FCB] Características
					*_ como
						*[#FFFFFF] Las matemáticas son base para su funcionamiento y desarrollo.
						*[#FFFFFF] No se pueden representar las finitas cifras que los forman.
						*[#FFFFFF] Son el mundo físico de los números.
			*_ tiene
				*[#C59FCB] Representación de \nnúmeros en el ordenador
					*_ la cual es
						*[#FFFFFF] La Codificación binaria
							*_ consiste
								*[#FFFFFF] Letras.
								*[#FFFFFF] Signos de puntuación.
								*[#FFFFFF] Números
									*_ para
										*[#FFFFFF] Representación en magnitud.
										*[#FFFFFF] Representación en exceso.
										*[#FFFFFF] Representación en complemento dos.

	*_ la
		*[#732487] <font color=#FFFFFF>Aritmetica del computador
			*_ tiene
				*[#C59FCB] Definición
					*_ es la
						*[#FFFFFF] Representación hábil de un número con infinidades.
						*[#FFFFFF] Cifras decimales en número de finitas posiciones.
				*[#C59FCB] Características
					*_ las cuales son
						*[#FFFFFF] El número real pierde su originalidad.
						*[#FFFFFF] Se trabaja con números decimales.
						*[#FFFFFF] Idea de error.
						*[#FFFFFF] Dígitos significativos.
						*[#FFFFFF] Truncar y redondear un número
							*_ son 
								*[#FFFFFF] Aspectos matemáticos de la aritmética finita.
			*_ la
				*[#C59FCB] Aproximación de un número
					*_ son
						*[#FFFFFF] Errores 
							*_ se comenten cuando
								*[#FFFFFF] se aproxima un número real o fraccionario con un número finito de cifras.
			*_ los
				*[#C59FCB] Dígitos significativos
					*_ son
						*[#FFFFFF] Números
						*[#FFFFFF] Dígitos
					*_ proporcionan 
						*[#FFFFFF] Información sobre la magnitud del signo.
				*[#C59FCB] Números con expresión finita
					*_ tiene
						*[#FFFFFF] Truncamiento
							*_ consisten en
								*[#FFFFFF] Despreciar un determinado número de cifras.
						*[#FFFFFF] Redondeo
							*_ consisten en
								*[#FFFFFF] Despreciar y retocar
									*_ la 
										*[#FFFFFF] Última cifra que no despreciamos de un modo adecuado.
			*_ representan
				*[#C59FCB] Números muy grandes o muy pequeños
					*_ en forma
						*[#FFFFFF] Del producto de un número\n por una potencia de 10
							*_ permite
								*[#FFFFFF] Dar una idea de la\n magnitud del número.
							*_ conservar
								*[#FFFFFF] Un orden del número.

	*_ el
		*[#732487] <font color=#FFFFFF> Sistema del computador 
			*_ tiene
				*[#C59FCB] Sistema binario
					*_ tiene
						*[#FFFFFF] Características
							*_ son
								*[#FFFFFF] Sistema más simple.
								*[#FFFFFF] Enlaza con la lógica newtoniana.
								*[#FFFFFF] Pertenencia en conjuntos.
								*[#FFFFFF] Dualidad 0,1
						*[#FFFFFF] Adición binaria
							*_ es
								*[#FFFFFF] Sumar en base 2, adaptando los números.
				*[#C59FCB] Sistema octal y decimal
					*_ tiene
						*[#FFFFFF] Representaciones completas de números.
				*[#C59FCB] Posiciones de números
					*[#FFFFFF] Dígitos binarios
						*_ tiene
							*[#FFFFFF] Representación de números.
							*[#FFFFFF] Representación entera de números enteros.
							*[#FFFFFF] Representación en exceso.
							*[#FFFFFF] Representación en magnitud signo.
							*[#FFFFFF] Representación en complemento dos.
						*_ poseen
							*[#FFFFFF] Ideas matemáticas
								*[#FFFFFF] Convertir ceros y unos en una \nposición de memoria en un \nnúmero entero.
							*[#FFFFFF] Número entero
								*[#FFFFFF] Forma de circuitos por el que pasa \ny no pasa la corriente.

	*_ la
		*[#732487] <font color=#FFFFFF> Aritmética en punto flotante 
			*_ es
				*[#C59FCB] Sumar, multiplicar, restar números que \nsean representados con una condición científica.
					*_ ejemplo
						*[#FFFFFF] Calculadoras científicas
@endmindmap
```









